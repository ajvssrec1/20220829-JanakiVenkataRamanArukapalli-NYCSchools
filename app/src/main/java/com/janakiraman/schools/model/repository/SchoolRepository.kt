package com.janakiraman.schools.model.repository

import com.janakiraman.schools.model.api.APIClient.apiInterface
import com.janakiraman.schools.model.data.School
import com.janakiraman.schools.model.data.SchoolDetails
import retrofit2.Response

class SchoolRepository {

    suspend fun getSchoolsAPIResponse(): Response<ArrayList<School>> {
        return apiInterface.getSchoolsResponse()
    }

    suspend fun getSchoolDetailsAPIResponse(dbnClicked: String): Response<Array<SchoolDetails>> {
        return apiInterface.getSchoolDetails(dbnClicked)
    }

}