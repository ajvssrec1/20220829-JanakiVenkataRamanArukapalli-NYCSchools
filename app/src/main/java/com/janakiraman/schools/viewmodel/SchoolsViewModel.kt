package com.janakiraman.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.janakiraman.schools.model.data.School
import com.janakiraman.schools.model.repository.SchoolRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.Response

class SchoolsViewModel(val schoolsRepository: SchoolRepository = SchoolRepository()) : ViewModel() {

    var schoolsLiveData = MutableLiveData<ArrayList<School>>()
    var errorLiveDate = MutableLiveData<Response>()
    private var job: Job? = null
    fun getSchools() {
        job = viewModelScope.launch {
            val response = schoolsRepository.getSchoolsAPIResponse()
            if (response.code().equals(200)) {
                schoolsLiveData.postValue(response.body())
            } else if (response.code().equals(400..500)) {
                errorLiveDate.postValue(response.raw())
            }
        }
    }
}
