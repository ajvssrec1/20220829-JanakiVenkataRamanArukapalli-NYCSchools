package com.janakiraman.schools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.janakiraman.schools.model.data.SchoolDetails
import com.janakiraman.schools.model.repository.SchoolRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SchoolDetailsViewModel : ViewModel() {

    var schoolDetailsLiveData = MutableLiveData<SchoolDetails?>()
    private var schoolsRepository = SchoolRepository()
    private var job: Job? = null
    fun getSchoolDetails(dbnClicked: String) {
        job = viewModelScope.launch {
            val response = schoolsRepository.getSchoolDetailsAPIResponse(dbnClicked)
            if (response.isSuccessful) {
                val schoolDetails = response.body()
                if (schoolDetails != null && schoolDetails.size > 0)
                    schoolDetailsLiveData.postValue(schoolDetails[0])
                else
                    schoolDetailsLiveData.postValue(null)
            }
        }
    }
}