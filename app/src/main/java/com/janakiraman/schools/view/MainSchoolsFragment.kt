package com.janakiraman.schools.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.janakiraman.schools.R
import com.janakiraman.schools.databinding.FragmentMainSchoolsBinding
import com.janakiraman.schools.model.data.School
import com.janakiraman.schools.view.adapter.OnItemClickListener
import com.janakiraman.schools.view.adapter.SchoolsAdapter
import com.janakiraman.schools.viewmodel.SchoolsViewModel

class MainSchoolsFragment : Fragment(), OnItemClickListener {

    private lateinit var schoolsViewModel: SchoolsViewModel
    private lateinit var schoolsAdapter: SchoolsAdapter

    //Setting the default value to be null , it should be null on recreation as well
    private var _binding: FragmentMainSchoolsBinding? = null

    //Getting the non null value of Binding
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolsViewModel = ViewModelProvider(this)[SchoolsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainSchoolsBinding.inflate(inflater, container, false)
        binding.progressBar.visibility = View.VISIBLE
        binding.schoolsList.layoutManager = LinearLayoutManager(binding.root.context)
        schoolsAdapter = SchoolsAdapter(this)
        binding.schoolsList.adapter = schoolsAdapter
        schoolsViewModel.getSchools()
        bindView()
        return binding.root
    }

    private fun bindView() {
        schoolsViewModel.schoolsLiveData.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = View.GONE
            schoolsAdapter.setSchools(it)
        })
        schoolsViewModel.errorLiveDate.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = View.GONE
            Toast.makeText(this.activity, it.message(), Toast.LENGTH_LONG).show()
        })
    }

    override fun onItemClicked(school: School) {
        Log.i("School DNB ", school.dbn + "  -  " + school.school_name)
        val bundle = bundleOf("dbnClicked" to school.dbn)
        view?.let { Navigation.findNavController(it) }
            ?.navigate(R.id.from_schoollistfragment_to_schooldetailsfragment, bundle)
    }
}