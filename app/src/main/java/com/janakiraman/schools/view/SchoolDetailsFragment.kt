package com.janakiraman.schools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.janakiraman.schools.R
import com.janakiraman.schools.databinding.FragmentSchoolDetailsBinding
import com.janakiraman.schools.model.data.SchoolDetails
import com.janakiraman.schools.viewmodel.SchoolDetailsViewModel

class SchoolDetailsFragment : Fragment() {

    private lateinit var schoolDetailsViewModel: SchoolDetailsViewModel

    //Setting the default value to be null , it should be null on recreation as well
    private var _binding: FragmentSchoolDetailsBinding? = null

    //Getting the non null value of Binding
    private val binding get() = _binding!!

    companion object;

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSchoolDetailsBinding.inflate(inflater, container, false)
        binding.progressBar.visibility = View.VISIBLE
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        schoolDetailsViewModel = ViewModelProvider(this)[SchoolDetailsViewModel::class.java]
        arguments?.getString("dbnClicked")?.let { schoolDetailsViewModel.getSchoolDetails(it) }
        bindView()
    }

    private fun bindView() {
        schoolDetailsViewModel.schoolDetailsLiveData.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = View.GONE
            mapContents(it)
        })
    }

    private fun mapContents(it: SchoolDetails?) {
        if (it != null) {
            binding.titleMathAvg.visibility = View.VISIBLE
            binding.titleReadingAvg.visibility = View.VISIBLE
            binding.titleTakers.visibility = View.VISIBLE
            binding.titleWritingAvg.visibility = View.VISIBLE
            binding.schoolDetails = it
        } else {
            Toast.makeText(
                this.activity,
                this.getString(R.string.on_empty_response),
                Toast.LENGTH_LONG
            ).show()
        }
    }

}