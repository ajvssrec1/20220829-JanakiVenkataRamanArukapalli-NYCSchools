package com.janakiraman.schools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.janakiraman.schools.model.data.School
import com.janakiraman.schools.model.repository.SchoolRepository
import com.janakiraman.schools.viewmodel.SchoolsViewModel
import io.mockk.coEvery
import io.mockk.impl.annotations.SpyK
import io.mockk.spyk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response


@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SchoolsViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()
    private lateinit var viewModel: SchoolsViewModel
    private lateinit var school: School

    @SpyK
    private var schoolRepository: SchoolRepository = spyk()

    @Before
    fun setup() {
        viewModel = SchoolsViewModel(schoolRepository)
    }

    @Test
    fun onSuccessReturnListOfSchools() {

        school = School(
            "123abc",
            "Star Union School",
            "325 Oak Creek Dr",
            "Hoffman Estates",
            "IL",
            "94536"
        )
        val schoolList = arrayListOf<School>(school)
        coEvery { schoolRepository.getSchoolsAPIResponse() }.returns(
            Response.success(
                schoolList
            )
        )
        runBlocking {

            viewModel.getSchools()
            Assert.assertEquals(1, viewModel.schoolsLiveData.value?.size)
        }
    }

    @Test
    fun onFailureReturnError() {
        coEvery { schoolRepository.getSchoolsAPIResponse() }.returns(Response.error(400,
            ResponseBody.create(MediaType.parse("type"), "")))
        runBlocking {
            viewModel.getSchools()
            Assert.assertEquals(null,viewModel.schoolsLiveData.value?.size)
        }
    }

}